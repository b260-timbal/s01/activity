<?php
//Full Address function
function getFullAddress($street, $city, $province, $country) {
    return "$street, $city, $province, $country";
}

// Letter Grade function
function getLetterGrade($grade){
    if ($grade > 97 && $grade <=100) {
        return "$grade is equivalent to A+";
    } else if ($grade > 94 && $grade <=97) {
        return "$grade is equivalent to A";
    } else if ($grade > 91 && $grade <=94) {
        return "$grade is equivalent to A-";
    } else if ($grade > 88 && $grade <=91) {
        return "$grade is equivalent to B+";
    } else if ($grade > 85 && $grade <= 88) {
        return "$grade is equivalent to B";
    } else if ($grade > 82 && $grade <=85) {
        return "$grade is equivalent to B-";
    } else if ($grade > 79 && $grade <=82) {
        return "$grade is equivalent to C+";
    } else if ($grade > 76 && $grade <=79) {
        return "$grade is equivalent to C";
    } else if ($grade > 74 && $grade <=76) {
        return "$grade is equivalent to C-";
    } else if ($grade <= 74) {
        return "$grade is equivalent to F";
    } else {
        return "Please check your input";
    }

};
?>